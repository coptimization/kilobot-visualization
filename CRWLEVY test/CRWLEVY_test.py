from graphics import *
import time
import random
import math
import subprocess

# Test parameters
num_steps = 45
crw_exponent = 1.8
levy_exponent = 0.7
result_values = []
angle_values = []
step_lenght_values = []

# Path to files
crwlevy_parameters_path = "parameters_crwlevy.txt"
crwlevy_run_path = "gcc run_CRWLEVY.c -o run_CRWLEVY -lm;./run_CRWLEVY"
crwlevy_results_path = "results_crwlevy.txt"

# Draw parameters
frequency = 0.7
width = 1600
height = 900
start_point = Point(800, 450)
vertical_step = 18
draw_scale = 0.2

def runCRWLEVY():
    print("Py - Calling CRWLEVY test")
    while True:
        with open(crwlevy_parameters_path, "w+") as parameters:
            try:
                parameters.truncate(0)
                parameters.write("%.2f\n" % crw_exponent)
                parameters.write("%.2f\n" % levy_exponent)
                parameters.close()
                break
            except:
                print("Py - Couldnt open parameters file")
    called_crwlevy = False
    while not called_crwlevy:
        try:
            s = subprocess.check_call(crwlevy_run_path, shell=True)
            called_crwlevy = True
        except:
            print("Py - Couldnt call crwlevy test")
            time.sleep(1)

def getResults():
    finish = False
    while not finish:
        if os.path.getsize(crwlevy_results_path) != 0:
            finish = True
        time.sleep(0.2)

    read = False
    while not read:
        with open(crwlevy_results_path, "r+") as results_file:
            try:
                for i in range(num_steps):
                    result_values.append(results_file.readline())
                results_file.close()
                read = True
            except:
                print("Py - Couldnt read the results")
                time.sleep(1)
    for i in result_values:
        result = i.split(" ")
        angle_values.append(float(result[0]))
        step_lenght_values.append(int(result[1]))
        if (random.randint(0,1) == 1):
            angle_values[-1] = -angle_values[-1]

def moveLines(lines):
    if lines != []:
        dx = lines[-1].p1.getX() - lines[-1].p2.getX()
        dy = lines[-1].p1.getY() - lines[-1].p2.getY()
        for i in lines:
            i.undraw()
            i.move(dx, dy)
            i.draw(win)

def drawResults():
    step = 0
    robot_angle = 0
    values_text = []
    crwlevy_trajectory = []
    step_text = Text(Point(75, 880), str(step))
    time_before = time.time()
    while (step < num_steps):
        if (time.time() - time_before > frequency):
            first_point = start_point
            robot_angle += angle_values[step]
            second_point = Point(first_point.getX() + draw_scale*(step_lenght_values[step] *math.cos(robot_angle)), 
                first_point.getY() + draw_scale*(step_lenght_values[step] *math.sin(robot_angle)))
            moveLines(crwlevy_trajectory)
            crwlevy_trajectory.append(Line(first_point, second_point))
            values_text.append(Text(Point(50, 820 - (vertical_step *step)), str(angle_values[step]) + " " + str(step_lenght_values[step])))
            step_text.setText(str(step + 1))
            step_text.undraw()
            crwlevy_trajectory[step].draw(win)
            values_text[step].draw(win)
            step_text.draw(win)
            time_before = time.time()
            step += 1
    time.sleep(3)

    for i in values_text:
        i.undraw()
    for i in crwlevy_trajectory:
        i.undraw()
    step_text.undraw()


# Windows parameters
win = GraphWin("CRWLEVY Test", width, height, autoflush=True)
win.setBackground("white")
win.setCoords(0, 0, width, height)
step_number_text = Text(Point(25, 880), "Step: ")
crwlevy_values_text = Text(Point(75, 850), "Angle - Step Lenght")
step_number_text.draw(win)
crwlevy_values_text.draw(win)

runCRWLEVY()
getResults()
drawResults()

win.close()