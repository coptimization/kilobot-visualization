#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <float.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#define M2_PI (2.0*M_PI)

char *parameters_file = "parameters_crwlevy.txt";
char *results_file = "results_crwlevy.txt";

// Simulation parameters //
int max_steps = 50;

// CRWLEVY parameters //
double crw_exponent;
double levy_exponent;
const double std_motion_steps = 5*16;

// CRWLEVY functions //

double uniform_distribution(double a, double b){
  return (rand() * (b - a) + a) / ((double)RAND_MAX + 1);
}

double wrapped_cauchy_ppf(const double c){
  double val, theta, u, q;
  q = 0.5;
  u = uniform_distribution(0.0, 1.0);
  val = (1.0 - c) / (1.0 + c);
  theta = 2 * atan(val * tan(M_PI * (u - q)));
  return theta;
}

double exponential_distribution(double lambda){
  double u, x;
  u = uniform_distribution(0.0, 1.0);
  x = (-lambda) * log(1 - u);
  return (x);
}

/* The stable Levy probability distributions have the form

   p(x) dx = (1/(2 pi)) \int dt exp(- it x - |c t|^alpha)

   with 0 < alpha <= 2. 

   For alpha = 1, we get the Cauchy distribution
   For alpha = 2, we get the Gaussian distribution with sigma = sqrt(2) c.

   */

int levy(const double c, const double alpha){
  double u, v, t, s;
  u = M_PI * (uniform_distribution(0.0, 1.0) - 0.5); /*vedi uniform distribution */

  /* cauchy case */
  if (alpha == 1) {
    t = tan(u);
    return (int)(c * t);
  }
  do {
    v = exponential_distribution(1.0); /*vedi esponential distribution */
  } while (v == 0);

  /* gaussian case */
  if (alpha == 2) {
    t = 2 * sin(u) * sqrt(v);
    return (int)(c * t);
  }

  /* general case */
  t = sin(alpha * u) / pow(cos(u), 1 / alpha);
  s = pow(cos((1 - alpha) * u) / v, (1 - alpha) / alpha);
  return (int)(c * t * s);
}

void getCRWLEVYParameters(){
    // Open and read the parameters file
	char ch;
	FILE *fptr = fopen(parameters_file, "r");
	if(fptr == NULL){
			printf("C - Cannot open parameters_crwlevy.txt file\n");
			exit(0);
	}
    else{
        fseek(fptr, 0, SEEK_END); 
	    long fsize = ftell(fptr);
        if (fsize == 0){
            printf("C - File is empty");
            exit(0);
        }
    }
    fseek(fptr, 0, SEEK_SET);
    char buffer_crw[10];
    for(int i = 0; i < 10; i++){
        ch = fgetc(fptr);
        if(ch != '\n')
            buffer_crw[i] = ch;
        else
            break;
    }
    crw_exponent = atof(buffer_crw);
    printf("CRW parameters is: %.2f\n", crw_exponent);
    char buffer_levy[10];
    for(int i = 0; i < 10; i++){
        ch = fgetc(fptr);
        if(ch != '\n')
            buffer_levy[i] = ch;
        else
            break;
    }
    levy_exponent = atof(buffer_levy);
    printf("Levy parameters is: %.2f\n", levy_exponent);
    fclose(fptr);
}

void getValues(double *angle, int *step_lenght){
    if (crw_exponent == 0){
        *angle = (uniform_distribution(0, (M_PI)));
    }
    else
    {
        *angle = fabs(wrapped_cauchy_ppf(crw_exponent));
    }
    *angle = *angle * (180/M_PI);
    *step_lenght = (fabs(levy(std_motion_steps, levy_exponent)));
}

void looping(){
    double angle = 0;
    int step_lenght = 0;
    FILE *fptr = fopen(results_file, "w");
    for (int step = 0; step < max_steps; step++){
        getValues(&angle, &step_lenght);
        fprintf(fptr, "%.2f %d\n", angle, step_lenght);
    }
    fclose(fptr);
}

int main(){
    printf("C - Running CRWLEVY test\n");
    getCRWLEVYParameters();
    looping();
    printf("C - Run finished\n");
    return 0;
}