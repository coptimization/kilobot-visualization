from graphics import *
import time
import random
import math
import subprocess

# Test parameters
num_steps = 120
num_sim = 5

# RBN parameters
num_nodes = 16
num_boolfunction = num_nodes - 1
bf_maxvalue = 2
bf_minvalue = 0
min_connections = 3

# Path to files
rbn_read_path = "rbn_individual.txt"
rbn_parameters_path = "parameters_rbn.txt"
rbn_run_path = "gcc run_rbn.c -o run_rbn -lm;./run_rbn"
rbn_results_path = "results_rbn.txt"

# Draw parameters
frequency = 0.2
width = 1600
height = 900
start_point = Point(800, 450)
vertical_step = 18
draw_scale = 0.1
limit_step_on_screen = 45

# Class
class RBN:
    def __init__(self, connections, boolfunctions):
        self.connections = connections
        self.boolfunctions = boolfunctions
        self.values = []
        self.angle_values = []
        self.step_lenght_values = []
        self.step_length_type = []
        self.step_node_values = []

    def setValues(self, n_values):
        self.values.append(n_values.rstrip())

    def getAngleValues(self, step):
        return self.angle_values[step]

    def getSlValues(self, step):
        sl = str(self.step_lenght_values[step]) + " => " + self.step_length_type[step]
        return sl
    
    def deleteValues(self):
        self.values = []
        self.angle_values = []
        self.step_lenght_values = []
        self.step_length_type = []
        self.step_node_values = []

    def calculateTrajectory(self):
        for i in self.values:
            result = i.split(" ")
            self.angle_values.append(float(result[0]))
            self.step_lenght_values.append(int(result[1]))
            self.step_length_type.append(result[2])

# Functions
def readRBN():
    print("Py - Reading RBN Individual")
    connections = []
    booleanfunctions = []
    while True:
        with open(rbn_read_path, "r+") as parameters:
            try:
                n_nodes = int(parameters.readline())
                n_boolfunction = n_nodes - 1
                for i in range(n_nodes):
                    connections.append([])
                    line = parameters.readline()
                    for ch in line:
                        if ch != '\n':
                            connections[i].append(int(ch))
                for i in range(n_nodes):
                    booleanfunctions.append([])
                    line = parameters.readline()
                    for ch in line:
                        if ch != '\n':
                            booleanfunctions[i].append(int(ch))
                parameters.close()
                break
            except:
                print("Py - Couldnt open rbn read file")
                time.sleep(1)
    individual = RBN(connections, booleanfunctions)
    return individual, n_nodes, n_boolfunction

def createInitialState():
    initial_states = []
    first_state = [0]*num_nodes
    second_state = [1]*num_nodes
    initial_states.append(first_state)
    initial_states.append(second_state)
    for i in range(num_sim - 2):
        init_state = []
        for j in range(num_nodes):
            init_state.append(random.randint(0,1))
        initial_states.append(init_state)
    return initial_states

def runRBN(rbn, init_state):
    print("Py - Calling RBN test")
    while True:
        with open(rbn_parameters_path, "w+") as parameters:
            try:
                parameters.truncate(0)
                parameters.write("%d\n" % num_nodes)
                parameters.write("%d\n" % num_steps)
                for i in range(num_nodes):
                    for j in range(num_nodes):
                        parameters.write("%d" % rbn.connections[i][j])
                    parameters.write("\n")
                for i in range(num_nodes):
                    for j in range(num_boolfunction):
                        parameters.write("%d" % rbn.boolfunctions[i][j])
                    parameters.write("\n")
                for i in range(num_nodes):
                    parameters.write("%d" % init_state[i])
                parameters.close()
                break
            except AssertionError as error:
                print(error)
                print("Py - Couldnt open parameters file")
                time.sleep(1)
    called_rbn = False
    while not called_rbn:
        try:
            s = subprocess.check_call(rbn_run_path, shell=True)
            called_rbn = True
        except:
            print("Py - Couldnt call RBN test")
            time.sleep(1)

def getResults(rbn):
    finish = False
    while not finish:
        if os.path.getsize(rbn_results_path) != 0:
            finish = True
        time.sleep(0.2)

    read = False
    while not read:
        with open(rbn_results_path, "r+") as results_file:
            try:
                for i in range(num_steps):
                    rbn.setValues(results_file.readline())
                results_file.truncate(0)
                results_file.close()
                read = True
            except:
                print("Py - Couldnt read the results")
                time.sleep(1)

def moveLines(lines):
    if lines != []:
        dx = lines[-1].p1.getX() - lines[-1].p2.getX()
        dy = lines[-1].p1.getY() - lines[-1].p2.getY()
        for i in lines:
            i.undraw()
            i.move(dx, dy)
            i.draw(win)

def drawResults(rbn, init_state):
    step = 0
    robot_angle = 0
    initial_state_text = Text(Point(800, 850), str(init_state))
    initial_state_text.draw(win)
    angle_values_text = []
    sl_values_text = []
    rbn_trajectory = []
    step_text = Text(Point(55, 880), str(step))
    rbn.calculateTrajectory()
    time_before = time.time()
    while (step < num_steps):
        if (win.checkMouse() != None):
            win.getMouse()
        if (time.time() - time_before > frequency):
            if (step > limit_step_on_screen):
                angle_values_text[0].undraw()
                angle_values_text.pop(0)
                sl_values_text[0].undraw()
                sl_values_text.pop(0)
                for i in angle_values_text:
                    i.move(0, vertical_step)
                for i in sl_values_text:
                    i.move(0, vertical_step)
            else:
                values_posy = vertical_step *step
        if (time.time() - time_before > frequency):
            first_point = start_point
            robot_angle += rbn.angle_values[step]
            second_point = Point(first_point.getX() + draw_scale*(rbn.step_lenght_values[step] *math.cos(robot_angle)), 
                first_point.getY() + draw_scale*(rbn.step_lenght_values[step] *math.sin(robot_angle)))
            moveLines(rbn_trajectory)
            rbn_trajectory.append(Line(first_point, second_point))
            angle_values_text.append(Text(Point(45, 830 - values_posy), rbn.getAngleValues(step)))
            sl_values_text.append(Text(Point(135, 830 - values_posy), rbn.getSlValues(step)))
            step_text.setText(str(step + 1))
            step_text.undraw()
            rbn_trajectory[-1].draw(win)
            angle_values_text[-1].draw(win)
            sl_values_text[-1].draw(win)
            step_text.draw(win)
            time_before = time.time()
            step += 1
    win.getMouse()

    for i in angle_values_text:
        i.undraw()
    for i in sl_values_text:
        i.undraw()
    for i in rbn_trajectory:
        i.undraw()
    step_text.undraw()
    initial_state_text.undraw()
    rbn.deleteValues()


# Windows parameters
win = GraphWin("RBN Test", width, height, autoflush=True)
win.setBackground("white")
win.setCoords(0, 0, width, height)
step_number_text = Text(Point(25, 880), "Step: ")
angle_text = Text(Point(45, 850), "Angle")
sl_text = Text(Point(135, 850), "Step Lenght")
step_number_text.draw(win)
angle_text.draw(win)
sl_text.draw(win)

rbn, num_nodes, num_boolfunction = readRBN()
initial_states = createInitialState()
for init_state in initial_states:
    runRBN(rbn, init_state)
    getResults(rbn)
    drawResults(rbn, init_state)
win.close()