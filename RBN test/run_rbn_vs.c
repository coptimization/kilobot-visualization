#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <float.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#define M2_PI (2.0*M_PI)

char *parameters_file = "parameters_rbn.txt";
char *results_file = "results_rbn.txt";

/* Set Random Boolean Network parameters*/
int num_nodes;
int num_boolfuntcions;
int sim_steps;
int max_step_value;
int short_step_lenght = 200;
int medium_step_lenght = 800;
int long_step_lenght = 9200;
const int num_nodes_vs = 4;
const int step_change_vs = 30;

/* Creating Random Boolean Network struct */
struct RBN{
	int *nodes_state;
    int *connections_number;
	int **connections;
	int **booleanfunctions;
};
struct RBN individual_rbn;

void printInitialStates(){
    for(int i = 0; i < num_nodes; i++)
        printf("%d", individual_rbn.nodes_state[i]);
    printf("\n");
}

/*-------------------------------------------------------------------*/
/* Random Boolean Network functions                                  */
/*-------------------------------------------------------------------*/

void allocateRBN(){
    individual_rbn.nodes_state = malloc(sizeof(int)*num_nodes);
    individual_rbn.connections_number = malloc(sizeof(int)*num_nodes);
    individual_rbn.connections = malloc(sizeof(int*)*num_nodes);
	for(int i = 0; i < num_nodes; i++)
		individual_rbn.connections[i] = malloc(sizeof(int)*num_nodes);
    individual_rbn.booleanfunctions = malloc(sizeof(int*)*num_nodes);
	for(int i = 0; i < num_nodes; i++)
		individual_rbn.booleanfunctions[i] = malloc(sizeof(int)*num_boolfuntcions);
}

void calculateConnectionsNum(){
    // Calculate the number of connections each node has
	for(int i = 0; i < num_nodes; i++){
		individual_rbn.connections_number[i] = 0;
		for(int j = 0; j < num_nodes; j++){
			if(individual_rbn.connections[i][j] > 0)
				individual_rbn.connections_number[i]++;
		}
    }
}

void setRBNparameters(){
	// Open and read the parameters file
	char ch;
	FILE *fptr = fopen(parameters_file, "r");
	if(fptr == NULL){
			printf("C - Cannot open parameters_rbn.txt file\n");
			exit(0);
	}
    else{
        fseek(fptr, 0, SEEK_END); 
	    long fsize = ftell(fptr);
        if (fsize == 0){
            printf("C - File is empty");
            exit(0);
        }
    }
    fseek(fptr, 0, SEEK_SET);
    num_nodes = (int)fgetc(fptr) - '0';
    ch = fgetc(fptr);
    if (ch != '\n'){
        num_nodes = num_nodes*10 + ((int)ch - '0');
        ch = fgetc(fptr);
    }
    num_boolfuntcions = num_nodes - 1;
    sim_steps = (int)fgetc(fptr) - '0';
    ch = fgetc(fptr);
    if (ch != '\n'){
        sim_steps = sim_steps*10 + ((int)ch - '0');
        ch = fgetc(fptr);
    }
    allocateRBN();
    max_step_value = pow(2, ((num_nodes-num_nodes_vs)/2));
    printf("C - Num. nodes = %d, Max. step length = %d and Simulation steps = %d\n", num_nodes, max_step_value, sim_steps);
    // Set the connections value, booleanfuntions value and nodes state
	for(int i = 0; i < num_nodes; i++){
		for(int j = 0; j < num_nodes; j++){
			ch = fgetc(fptr);
			if(ch != '\n')
				individual_rbn.connections[i][j] = (int)ch - '0';
			else
				j = -1;
		}
	}
	for(int i = 0; i < num_nodes; i++){
		for(int j = 0; j < num_boolfuntcions; j++){
			ch = fgetc(fptr);
			if(ch != '\n')
				individual_rbn.booleanfunctions[i][j] = (int)ch - '0';
			else
				j = -1;
		}
	}
    for(int i = 0; i < num_nodes; i++){
        ch = fgetc(fptr);
        if (ch != '\n')
		    individual_rbn.nodes_state[i] = (int)ch - '0';
        else
            i = -1;
    }
    printInitialStates();
    calculateConnectionsNum();
	fclose(fptr);
}

void freeingRBN(){
	for(int i = 0; i < num_nodes; i++)
		free(individual_rbn.connections[i]);
	for(int i = 0; i < num_boolfuntcions; i++)
		free(individual_rbn.booleanfunctions[i]);
	free(individual_rbn.nodes_state);
    free(individual_rbn.connections_number);
	free(individual_rbn.booleanfunctions);
    free(individual_rbn.connections);
}

int logicmapRBN(int boolfunction, int node_a, int node_b){
	switch(boolfunction){
		case 0:
			return node_a & node_b; /*AND*/
		case 1:
			return node_a | node_b; /*OR*/
		case 2:
			return node_a ^ node_b; /*XOR*/
		case 3:
			return !(node_a ^ node_b); /*XNOR*/
		case 4:
			return !(node_a & node_b); /*NAND*/
		case 5:
			return !(node_a | node_b); /*NOR*/
		default:
			printf("C - Error in logicmap = %d\n", boolfunction);
			break;
	}
	return 0;
}

void calculateRbnNextStep(){
    // Calculate the next step for each node
    int* new_node_states = malloc(sizeof(int)*num_nodes);
    for(int i = 0; i < num_nodes; i++){
        // Get the state of each node connected to itself
        if(individual_rbn.connections_number[i] > 1){
            int* connected_nodes_state = malloc(sizeof(int)*num_nodes);
            int cont_nodes = 0;
            for(int j = 0; j < num_nodes; j++){
                if(individual_rbn.connections[i][j] == 1){
                    connected_nodes_state[cont_nodes] = individual_rbn.nodes_state[j];
                    cont_nodes++;
                }
                else if(individual_rbn.connections[i][j] == 2){
                    if(individual_rbn.nodes_state[j] == 0)
                        connected_nodes_state[cont_nodes] = 1;
                    else 
                        connected_nodes_state[cont_nodes] = 0;
                    cont_nodes++;
                }
            }
            // Calculate next node state
            int flogic_num = individual_rbn.connections_number[i] - 1;
            int *connected_state = malloc(sizeof(int)*(individual_rbn.connections_number[i] + flogic_num));
            for(int j = 0; j < individual_rbn.connections_number[i]; j++)
                connected_state[j] = connected_nodes_state[j];
            int cont_states = 0;
            for(int j = 0; j < flogic_num; j++){
                int aux_flogic = individual_rbn.booleanfunctions[i][j];
                int new_state = logicmapRBN(aux_flogic, connected_state[cont_states], connected_state[cont_states+1]);
                connected_state[individual_rbn.connections_number[i] + j] = new_state;
                cont_states += 2;
            }
            new_node_states[i] = connected_state[cont_states-1];
            free(connected_nodes_state);
            free(connected_state);
        }
        else
            new_node_states[i] = individual_rbn.nodes_state[i];
    }
    // Set the new states for each node
    for(int i = 0; i < num_nodes; i++)
        individual_rbn.nodes_state[i] = new_node_states[i];
    free(new_node_states);
}

void getValues(double *angle, int *step_lenght, char *type_sl, int step_nodes[6]){
    // Get the angle in rad
    int angle_direction = 0;
    int values_num = (num_nodes - num_nodes_vs)/2;
    int num_directions = pow(2, values_num);
    for(int i = 0; i < values_num; i++){
        if(individual_rbn.nodes_state[i] == 1)
            angle_direction += pow(2, i);
    }
    *angle = (angle_direction/(float)num_directions)*M_PI;
    *angle = *angle * (180/M_PI);
    if (rand() % 2 == 0)
        *angle = *angle *(-1);
    printf("%.2f\n", *angle);

    // Get the step lenght (distance it goes forward)
    int step_value = 0;
    for(int i = values_num; i < (num_nodes - num_nodes_vs); i++){
        step_nodes[i-values_num] = individual_rbn.nodes_state[i];
        if(individual_rbn.nodes_state[i] == 1)
            step_value += pow(2, i - values_num);
    }
    if(step_value < (round(max_step_value* 0.7))){
        *step_lenght = rand() % short_step_lenght;
        *type_sl = 's';
    }
    else if (step_value < (round(max_step_value* 0.9))){
        *step_lenght = (rand() % medium_step_lenght) + short_step_lenght;
        *type_sl = 'm';
    }
    else{
        *step_lenght = (rand() % long_step_lenght) + medium_step_lenght;
        *type_sl = 'l';
    }
}

void changeVS(){
    for(int i = num_nodes - num_nodes_vs; i < num_nodes; i++)
        individual_rbn.nodes_state[i] = 1;
}

void printRBN(){
    printf("Rbn Nodes value: ");
    for(int i = 0; i < num_nodes; i++){
        printf("%d", individual_rbn.nodes_state[i]);
    }
    printf("\n");
}

void looping(){
    double angle = 0;
    int step_lenght = 0;
    int step_nodes[num_nodes-num_nodes_vs];
    char type_sl;
    FILE *fptr = fopen(results_file, "w");
    for (int step = 0; step < sim_steps; step++){
        if (step == step_change_vs)
            changeVS();
        getValues(&angle, &step_lenght, &type_sl, step_nodes);
        fprintf(fptr, "%.2f %d %c\n", angle, step_lenght, type_sl);
        // printRBN();
        // for(int i = 0; i < 6; i++)
        //     fprintf(fptr, "%d", step_nodes[i]);
        //fprintf(fptr, "\n");
        calculateRbnNextStep();
    }
    fclose(fptr);
}

int main(){
    printf("C - Running RBN test\n");
    setRBNparameters();
    looping();
    freeingRBN();
    printf("C - Run finished\n");
    return 0;
}